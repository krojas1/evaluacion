$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $('.carousel').carousel({interval:1500});

    $('#registrate').on('show.bs.modal',function(e){
        console.log("Se muestra el modal");
        $('#registrateBtn').removeClass('btn-danger');
        $('#registrateBtn').addClass('btn-info');
        $('#registrateBtn').prop('disabled',true);
    });

    $('#registrate').on('shown.bs.modal',function(e){
        console.log('Se mostro el modal');
    });

    $('#registrate').on('hide.bs.modal',function(e){
        console.log("Se oculta el modal");
        $('#registrateBtn').removeClass('btn-info');
        $('#registrateBtn').addClass('btn-danger');
        $('#registrateBtn').prop('disabled',false);
    });

    $('#registrate').on('hidden.bs.modal',function(e){
        console.log("Se oculto el modal");
    });
});